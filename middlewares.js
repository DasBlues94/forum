const jwt = require("jsonwebtoken")
const config = require("./config")
/**
 * Middleware for Verification of jwt token
*/
const authenticateToken = (req, res, next) => {
    // console.log(req.headers)
  const token = req.headers.authorization
    // console.log(token)
  if (!token) {
    return res.status(401).send({
      error: true,
      message: "No token provided"
    })
  }
  return jwt.verify(token.split("Bearer ")[1], config.development.secret, (err, decoded) => {
      // console.log(err)
    if (err) {
      return res.status(403).send({
        error: true,
        message: "Failed to authenticate token"
      })
    }
    req.decoded = decoded
    return next()
  })
}
module.exports = authenticateToken
