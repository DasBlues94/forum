const mongoose = require("mongoose")

// const config = require('../config')[process.env.NODE_ENV || 'development'];

const DirectorySchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  dateOfCreation: {
    type: String,
    default: new Date().toISOString()
  },
  _categoryId: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category"
  }]
})

module.exports = mongoose.model("Directory", DirectorySchema)
