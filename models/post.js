const mongoose = require("mongoose")

// const config = require('../config')[process.env.NODE_ENV || 'development'];

const PostSchema = new mongoose.Schema({
  _directoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Directory"
  },
  _categoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category"
  },
  _threadId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Thread"
  },
  message: {
    type: String,
    required: true
  },
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  dateOfPost: {
    type: String,
    default: new Date().toISOString()
  },
  lastEdit: {
    type: String,
    default: new Date().toISOString()
  }
})

module.exports = mongoose.model("Post", PostSchema)
