const mongoose = require("mongoose")

// const config = require('../config')[process.env.NODE_ENV || 'development'];

const CategorySchema = new mongoose.Schema({
  _directoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Directory"
  },
  name: {
    type: String,
    unique: true,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  dateOfCreation: {
    type: String,
    default: new Date().toISOString()
  }
})

module.exports = mongoose.model("Category", CategorySchema)
