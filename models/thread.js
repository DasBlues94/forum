const mongoose = require("mongoose")

// const config = require('../config')[process.env.NODE_ENV || 'development'];

const ThreadSchema = new mongoose.Schema({
  _directoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Directory"
  },
  _categoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category"
  },
  title: {
    type: String,
    required: true
  },
  message: {
    type: String,
    required: true
  },
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  dateOfCreation: {
    type: String,
    default: new Date().toISOString()
  }
})

module.exports = mongoose.model("Thread", ThreadSchema)
