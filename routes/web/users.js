const jwt = require("jsonwebtoken")
const config = require("../../config")

/**
 * Import Models
 */
const User = require("../../models/user")

module.exports = {
  login: (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.body, "email") || !Object.prototype.hasOwnProperty.call(req.body, "password")) {
      return res.status(500).send({
        error: true,
        msg: "Empty Fields"
      })
    }
    return User.findOne({ email: req.body.email.trim() })
    .exec()
    .then((user) => {
      if (!user) {
        return res.status(500).send({
          error: true,
          msg: "No users found"
        })
      }
      return user.comparePassword(req.body.password, (err1, isMatch) => {
        if (err1) {
          return res.status(500).send({
            error: true,
            msg: err1.message
          })
        } else if (!isMatch) {
          return res.status(500).send({
            error: true,
            msg: "Password mismatch"
          })
        }
        const payload = {
          _id: user._id,
          email: user.email.trim(),
          isAdmin: user.isAdmin,
          isActive: user.isActive
        }
        const token = jwt
        .sign(payload, config.development.secret, { expiresIn: 86400 })
        return res.status(200).send({
          error: false,
          msg: "Logged in succesfully",
          token
        })
      })
    })
    .catch((err) => {
      if (err) {
        res.status(500).send({
          error: true,
          msg: err.message
        })
      }
    })
  },
  createUser: (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.body, "email") || !Object.prototype.hasOwnProperty.call(req.body, "password") || !Object.prototype.hasOwnProperty.call(req.body, "username")) {
      return res.status(500).send({
        error: true,
        msg: "Empty Fields"
      })
    }
    const createUser = new User({
      username: req.body.username.trim(),
      email: req.body.email.trim(),
      password: req.body.password,
      isAdmin: req.body.isAdmin
    })
    return createUser.save((err) => {
      if (err) {
        return res.status(500).send({
          error: true,
          msg: err.message
        })
      }
      return res.status(200).send({
        error: false,
        msg: "Successfully created"
      })
    })
  }
}
