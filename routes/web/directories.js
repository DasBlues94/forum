/**
 * Import Models
 */
const Directory = require("../../models/directory")
const Category = require("../../models/category")
const Thread = require("../../models/thread")
const Post = require("../../models/post")

module.exports = {
  createDirectory: (req, res) => {
    if (req.decoded.isAdmin) {
      return res.status(403).send({
        error: true,
        msg: "Not Authorized to create directory"
      })
    }
    if (!Object.prototype.hasOwnProperty.call(req.body, "name")) {
      return res.status(500).send({
        error: true,
        msg: "Empty fields"
      })
    }
    const createDir = new Directory({
      name: req.body.name.trim()
    })
    return createDir.save((err) => {
      if (err) {
        return res.status(500).send({
          error: true,
          msg: "Cannot create directory"
        })
      }
      return res.status(200).send({
        error: true,
        msg: "Successfully created directory"
      })
    })
  },
  editDirectory: (req, res) => {
    if (!req.decoded.isAdmin) {
      return res.status(403).send({
        error: true,
        msg: "Not Authorized to create directory"
      })
    }
    if (!Object.prototype.hasOwnProperty.call(req.body, "name")) {
      return res.status(500).send({
        error: true,
        msg: "Empty fields"
      })
    }
    return Directory.findById(req.params.id)
    .exec()
    .then((directory) => {
      directory.name = req.body.name.trim()
      return directory.save()
    })
    .then((directory) => {
      res.status(200).send({
        error: false,
        directory
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  showDirectoryDetails: (req, res) => {
    Directory.find({})
    .populate("_categoryId")
    .exec()
    .then((directories) => {
      res.status(200).send({
        error: false,
        directories,
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  deleteDirectory: (req, res) => {
    if (!req.decoded.isAdmin) {
      res.status(403).send({
        error: true,
        msg: "Not Authorized to delete directory"
      })
    }
    return Post.deleteMany({ _directoryId: req.params.id })
    .exec()
    .then(() => Thread.deleteMany({ _directoryId: req.params.id }).exec())
    .then(() => Category.deleteMany({ _directoryId: req.params.id }).exec())
    .then(() => Directory.findByIdAndRemove(req.params.id).exec())
    .then(() => {
      res.status(200).send({
        error: false,
        msg: "Successfully deleted directory and related items"
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  }
}
