/**
 * Import Models
 */
const Post = require("../../models/post")

module.exports = {
  createPost: (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.body, "message") || !Object.prototype.hasOwnProperty.call(req.body, "_threadId") || !Object.prototype.hasOwnProperty.call(req.body, "_categoryId") || !Object.prototype.hasOwnProperty.call(req.body, "_directoryId")) {
      return res.status(500).send({
        error: true,
        msg: "Empty fields"
      })
    }
    const createPost = new Post({
      _threadId: req.body._threadId,
      message: req.body.message.trim(),
      _userId: req.decoded._id,
      _categoryId: req.body._categoryId,
      _directoryId: req.body._directoryId
    })
    return createPost.save((err) => {
      if (err) {
        return res.status(500).send({
          error: true,
          msg: err.message
        })
      }
      return res.status(200).send({
        error: false,
        msg: "Successfully created a new post"
      })
    })
  },
  editPost: (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.body, "message")) {
      return res.status(500).send({
        error: true,
        msg: "Empty fields"
      })
    }
    return Post.findById(req.params.id)
    .exec()
    .then((post) => {
      if (post._userId.toString() === req.decoded._id
          || req.decoded.isAdmin) {
        post.message = req.body.message.trim()
        post.lastEdit = new Date().toISOString()
        return post.save()
      }
      throw new Error("Cannot Edit Comment")
    })
    .then((post) => {
      res.status(200).send({
        error: false,
        post
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  deletePost: (req, res) => {
    if (!req.decoded) {
      return res.status(403).send({
        error: true,
        msg: "Not verified to delete"
      })
    }
    return Post.findById(req.params.id)
    .exec()
    .then((post) => {
      console.log(!req.decoded.isAdmin)
      if ((post._userId.toString() === req.decoded._id)
          || req.decoded.isAdmin) {
        return Post.findByIdAndRemove(req.params.id)
      }
      throw new Error("Cannot delete post")
    })
    .then(() => {
      res.status(200).send({
        error: false,
        msg: "Successfully deleted"
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  }
}
