const express = require("express")
const router = express.Router()
const authenticateToken = require("../../middlewares")
const User = require("./users")
const Directory = require("./directories")
const Category = require("./categories")
const Thread = require("./threads")
const Post = require("./posts")

/**
* GET home page
*/
router.get("/", (req, res) => {
  res.render("index", { title: "Express" })
})

/**
 * For Users
 */
router.post("/login", User.login)
router.post("/createuser", User.createUser)

/**
 * For Directory
 */
router.post("/createdirectory", authenticateToken, Directory.createDirectory)
router.put("/editdirectory/:id", authenticateToken, Directory.editDirectory)
router.get("/getdirectorydetails", Directory.showDirectoryDetails)
router.delete("/deletedirectory/:id", authenticateToken, Directory.deleteDirectory)

/**
 * For Category
 */
router.post("/createcategory", authenticateToken, Category.createCategory)
router.put("/editcategory/:id", authenticateToken, Category.editCategory)
router.get("/showallcategory", Category.showAllCategory)
router.get("/showcategorydetail/:id", Category.showCategoryDetails)
router.delete("/deletecategory/:id", authenticateToken, Category.deleteCategory)
/**
 * For Thread
 */
router.post("/createthread", authenticateToken, Thread.createThread)
router.put("/editthread/:id", authenticateToken, Thread.editThread)
router.get("/getallthread", Thread.getAllThread)
router.get("/showthread/:id", Thread.showThread)
router.delete("/deletethread/:id", authenticateToken, Thread.deleteThread)

/**
 * For Post
 */
router.post("/createpost", authenticateToken, Post.createPost)
router.put("/editpost/:id", authenticateToken, Post.editPost)
router.delete("/deletepost/:id", authenticateToken, Post.deletePost)

module.exports = router
