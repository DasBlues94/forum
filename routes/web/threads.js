/**
 * Import Models
 */
const Thread = require("../../models/thread")
const Post = require("../../models/post")

module.exports = {
  createThread: (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.body, "title") || !Object.prototype.hasOwnProperty.call(req.body, "message") || !Object.prototype.hasOwnProperty.call(req.body, "_categoryId") || !Object.prototype.hasOwnProperty.call(req.body, "_directoryId")) {
      return res.status(500).send({
        error: true,
        msg: "Empty fields"
      })
    }
    const createThread = new Thread({
      title: req.body.title.trim(),
      message: req.body.message.trim(),
      _userId: req.decoded._id,
      _categoryId: req.body._categoryId,
      _directoryId: req.body._directoryId
    })
    return createThread.save((err) => {
      if (err) {
        return res.status(500).send({
          error: true,
          msg: err.message
        })
      }
      return res.status(200).send({
        error: false,
        msg: "Successfully created a new thread"
      })
    })
  },
  editThread: (req, res) => {
    if (!Object.prototype.hasOwnProperty.call(req.body, "title") || !Object.prototype.hasOwnProperty.call(req.body, "message")) {
      return res.status(500).send({
        error: true,
        msg: "Empty fields"
      })
    }
    return Thread.findById(req.params.id)
    .exec()
    .then((thread) => {
      if (thread._userId.toString() === req.decoded._id) {
        thread.title = req.body.title.trim()
        thread.message = req.body.message.trim()
        return thread.save()
      }
      throw new Error("Cannot edit thread")
    })
    .then((thread) => {
      res.status(200).send({
        error: false,
        thread
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  getAllThread: (req, res) => {
    Thread.find({})
    .exec()
    .then((thread) => {
      res.status(200).send({
        error: false,
        thread
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  showThread: (req, res) => {
    let threadDetail
    return Thread.findById(req.params.id)
    .populate("_categoryId")
    .populate("_userId", "username")
    .exec()
    .then((thread) => {
      threadDetail = thread
      return Post.find({ _threadId: thread._id })
                    .populate("_userId", "username")
                    .exec()
    })
    .then((posts) => {
      res.status(200).send({
        error: false,
        threadDetail,
        posts
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  deleteThread: (req, res) => {
    if (!req.decoded.isAdmin) {
      return res.status(403).send({
        error: true,
        msg: "Not authenticated to delete"
      })
    }
    return Post.deleteMany({ _threadId: req.params.id })
    .exec()
    .then(() => Thread.findByIdAndRemove(req.params.id).exec())
    .then(() => {
      res.status(200).send({
        error: true,
        msg: "Successfully deleted thread and related posts"
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: false,
        msg: err.message
      })
    })
  }
}
