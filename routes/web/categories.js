/**
 * Import Models
 */
const Directory = require("../../models/directory")
const Category = require("../../models/category")
const Thread = require("../../models/thread")
const Post = require("../../models/post")

module.exports = {
  createCategory: (req, res) => {
    if (!req.decoded.isAdmin) {
      return res.status(403).send({
        error: true,
        msg: "Not Authorized to create category"
      })
    }
    if (!Object.prototype.hasOwnProperty.call(req.body, "name") || !Object.prototype.hasOwnProperty.call(req.body, "description") || !Object.prototype.hasOwnProperty.call(req.body, "_directoryId")) {
      return res.status(500).send({
        error: true,
        msg: "Empty fields"
      })
    }
    const createCategory = new Category({
      _directoryId: req.body._directoryId,
      name: req.body.name.trim(),
      description: req.body.description.trim()
    })
    return createCategory.save((err, category) => {
      if (err) {
        return res.status(500).send({
          error: true,
          msg: "Cannot create category"
        })
      }
      return Directory.findById(req.body._directoryId)
      .exec()
      .then((directory) => {
        directory._categoryId.push(category)
        return directory.save()
      })
      .then(() => {
        res.status(200).send({
          error: false,
          msg: "Successfully created category"
        })
      })
      .catch((err1) => {
        res.status(500).send({
          error: true,
          msg: err1.message
        })
      })
    })
  },
  editCategory: (req, res) => {
    if (!req.decoded.isAdmin) {
      return res.status(403).send({
        error: true,
        msg: "Not Authorized to edit category"
      })
    }
    if (!Object.prototype.hasOwnProperty.call(req.body, "name") || !Object.prototype.hasOwnProperty.call(req.body, "description") || !Object.prototype.hasOwnProperty.call(req.body, "_directoryId")) {
      return res.status(500).send({
        error: true,
        msg: "Empty fields"
      })
    }
    return Category.findById(req.params.id)
    .exec()
    .then((category) => {
      category._directoryId = req.body._directoryId
      category.name = req.body.name.trim()
      category.description = req.body.description.trim()
      return category.save()
    })
    .then((category) => {
      res.status(200).send({
        error: false,
        category
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  showAllCategory: (req, res) => {
    Category.find({})
    .exec()
    .then((category) => {
      res.status(200).send({
        error: false,
        category
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  showCategoryDetails: (req, res) => {
    let category
    return Category.findById(req.params.id)
    .populate("_threadId")
    .exec()
    .then((c) => {
      category = c
      return Thread.find({ _categoryId: c._id }).exec()
    })
    .then((threads) => {
      console.log(threads)
      res.status(200).send({
        error: false,
        category,
        threads
      })
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  },
  deleteCategory: (req, res) => {
    if (!req.decoded.isAdmin) {
      res.status(403).send({
        error: true,
        msg: "Not Authorized to delete category"
      })
    }
    return Post.deleteMany({ _categoryId: req.params.id })
    .exec()
    .then(() => Thread.deleteMany({ _categoryId: req.params.id }).exec())
    .then(() => Category.findByIdAndRemove(req.params.id).exec())
    .then(() => Directory.findById({ _categoryId: req.params.id }).exec())
    .then((directory) => {
      directory._categoryId
      .splice(directory._categoryId.indexOf(req.params.id), 1)
      return directory.save()
    })
    .then(() => {
      res.status(200).send({
        error: false,
        msg: "Successfully deleted category and all related items"
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).send({
        error: true,
        msg: err.message
      })
    })
  }
}
